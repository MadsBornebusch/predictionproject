# Developed by Mads Bornebusch
# Technical University of Denmark (DTU) and Swinburne University of Technology 
# 2014


###  Prediction project ###
The code is released under the GNU General Public License.

Prediction project done in COS30018 Intelligent Systems on Swinburne University of Technology. 
The project is done by Ryan Byles, Nicholas Brogden, James Stephens and Mads Bornebusch. 

This project uses the .csv data files in the folder Data.  

In case of any questions regarding the code send an email to Mads Bornebusch: mads.bornebusch@gmail.com


### How to get it to work ###
To get this project to work three jar-files must be added to the Java build path. The libraries are:
	jcommon-1.0.14.jar
	jfreechart-1.0.13.jar
	pdm-timeseriesforecasting-ce-TRUNK-SNAPSHOT.jar
	weka.jar

They are all in this repository and can also be found in the weka installation. 
For the jar files other than weka.jar see instructions in the comment in the top of wekatest.jar. 
