
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import weka.classifiers.evaluation.NumericPrediction;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.functions.SMOreg;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.classifiers.timeseries.WekaForecaster;


// This is a modified version of the WEKA API example from: http://wiki.pentaho.com/display/DATAMINING/Time+Series+Analysis+and+Forecasting+with+Weka

/**
 * Example of using the time series forecasting API. To compile and
 * run the CLASSPATH will need to contain:
 *
 * weka.jar (from your weka distribution)
 * pdm-timeseriesforecasting-ce-TRUNK-SNAPSHOT.jar (from the time series package)
 * jcommon-1.0.14.jar (from the time series package lib directory)
 * jfreechart-1.0.13.jar (from the time series package lib directory)
 */
public class wekatest {

	private WekaForecaster forecaster = new WekaForecaster();


	public void train(String filename,String timestampHeader,String dataHeader, 
			int minLag, int maxLag, int timestampHeaderIndex, String timeFormat, String predictionModel) {
		try{
			// Load csv data
			CSVLoader loader = new CSVLoader();
			loader.setSource(new File(filename));
			
			if( !timeFormat.equals("Consecutive numbering")){
				loader.setDateFormat(timeFormat);
				loader.setDateAttributes(String.valueOf(++timestampHeaderIndex)); 
			}
		
			Instances data = loader.getDataSet();
			
						
			// Set the targets we want to forecast. This method calls setFieldsToLag() on the lag maker object for us
			forecaster.setFieldsToForecast(dataHeader);

			// Choose the model we will use for the forecaster
			if(predictionModel.equals("Multilayer model")){
				// Multilayer perceptron: http://weka.sourceforge.net/doc.dev/weka/classifiers/functions/MultilayerPerceptron.html
				System.out.println("Multilayer"); 
				MultilayerPerceptron neuralNetwork = new MultilayerPerceptron();
				neuralNetwork.setLearningRate(0.1); 
				neuralNetwork.setTrainingTime(700); 
				forecaster.setBaseForecaster(neuralNetwork);
			}else if (predictionModel.equals("SMO regression")){
				// SMO regression: http://weka.sourceforge.net/doc.dev/weka/classifiers/functions/SMOreg.html
				forecaster.setBaseForecaster(new SMOreg());
			}
			
			forecaster.getTSLagMaker().setTimeStampField(timestampHeader); // time stamp
			forecaster.getTSLagMaker().setMinLag(minLag);
			forecaster.getTSLagMaker().setMaxLag(maxLag);	

			
			// build the model
			forecaster.buildForecaster(data, System.out);

		}catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public ArrayList<Double> forecast(String filename, int numberOfForecasts, 
			int timestampHeaderIndex, String timeFormat) {

		// Forecasted data to return
		ArrayList<Double> forecastData = new ArrayList<Double>();
		try {


			// Load csv data
			CSVLoader loader = new CSVLoader();
			loader.setSource(new File(filename));
			
			if( !timeFormat.equals("Consecutive numbering")){
				loader.setDateFormat(timeFormat);
				loader.setDateAttributes(String.valueOf(++timestampHeaderIndex)); 
			}
			
			Instances data = loader.getDataSet();
			
			// Prime the forecaster with enough recent historical data to cover up to the maximum lag.
			forecaster.primeForecaster(data);

			// Forecast for numberOfForecasts units beyond the end of the training data
			List<List<NumericPrediction>> forecast = forecaster.forecast(numberOfForecasts, System.out);


			// Output the predictions. Outer list is over the steps; inner list is over the targets
			for (int i = 0; i < numberOfForecasts; i++) {
				List<NumericPrediction> predsAtStep = forecast.get(i);
				NumericPrediction predForTarget = forecast.get(i).get(0);
				forecastData.add((double)predForTarget.predicted());
			}

			// We can continue to use the trained forecaster for further forecasting by priming with the most recent historical data (as it becomes available).
			// At some stage it becomes prudent to re-build the model using current historical data.

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return forecastData;
	}
}

