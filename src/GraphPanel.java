import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

/* 
 * The origin of this file is http://stackoverflow.com/questions/8693342/drawing-a-simple-line-graph-in-java
 * It is edited for our purpose
*/

public class GraphPanel extends JPanel {

	private int border = 25;
    private int labelSize = 25;
    private Color lineColor = new Color(60, 100, 230, 180);
    private Color pointColor = new Color(60, 60, 255, 200);
    
    private Color lineColorExtra = new Color(230, 100, 60, 180);
    private Color pointColorExtra = new Color(255, 60, 60, 200);
    
    private Color gridColor = new Color(200, 200, 200, 200);
    private static final Stroke GRAPH_STROKE = new BasicStroke(1f);
    private int pointWidth = 3;
    private int numberYDivisions = 10;
    private List<Double> plotdata;
    private List<Double> plotdataExtra;

    public GraphPanel(List<Double> data, List<Double> dataExtra) {
        this.plotdata = data;
        this.plotdataExtra = dataExtra; 
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D g2d = (Graphics2D) graphics;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // Finding the x- and y-scale
        double xScale = ((double) getWidth() - (2 * border) - labelSize) / (plotdata.size() + plotdataExtra.size() - 1);
        
        // TODO Fix the y axis
        double yScale = ((double) getHeight() - 2 * border - labelSize) / (getMaxVal() - getMinVal());

        // Create a list and fill it with pixel points to plot
        List<Point> graphPoints = new ArrayList<>();
        for (int i = 0; i < plotdata.size(); i++) {
        	// x-axis is left to right
            int x1 = (int) (i * xScale + border + labelSize);
            // y-axis is top to bottom
            int y1 = (int) ((getMaxVal() - plotdata.get(i)) * yScale + border);
            // add to list
            graphPoints.add(new Point(x1, y1));
        }
        
     // Create a list with extra points and fill it with pixel points to plot
        List<Point> graphPointsExtra = new ArrayList<>();
        for (int i = 0; i < plotdataExtra.size(); i++) {
        	// x-axis is left to right
            int x1 = (int) (i * xScale + border + labelSize + plotdata.size() * xScale );
            // y-axis is top to bottom
            int y1 = (int) ((getMaxVal() - plotdataExtra.get(i)) * yScale + border);
            // add to list
            graphPointsExtra.add(new Point(x1, y1));
        }

        // Draw white background on graph area 
        g2d.setColor(Color.WHITE);
        // fillRect(x1,y1,x2,y2);
        g2d.fillRect(border + labelSize, border, getWidth() - (2*border) - labelSize, getHeight() - 2*border - labelSize);
        g2d.setColor(Color.BLACK);

        
        
        // create hatch marks and grid lines for y axis.
        for (int i = 0; i < numberYDivisions + 1; i++) {
            int x0 = border + labelSize;
            int x1 = pointWidth + border + labelSize;
            int y0 = getHeight() - ((i * (getHeight() - border * 2 - labelSize)) / numberYDivisions + border + labelSize);
            int y1 = y0;
            if (plotdata.size() > 0) {
            	//Horizontal grid lines
            	g2d.setColor(gridColor);
                g2d.drawLine(border + labelSize + 1 + pointWidth, y0, getWidth() - border, y1); 
                // Labels
                g2d.setColor(Color.BLACK);
                String yLabel = ((int) ((getMinVal() + (getMaxVal() - getMinVal()) * ((i * 1.0) / numberYDivisions)) * 100)) / 100.0 + "";
                FontMetrics metrics = g2d.getFontMetrics();
                int labelWidth = metrics.stringWidth(yLabel);
                g2d.drawString(yLabel, x0 - labelWidth - 5, y0 + (metrics.getHeight() / 2) - 3);
            }
            g2d.drawLine(x0, y0, x1, y1);
        }

        // and for x axis
        for (int i = 0; i < (plotdata.size()+plotdataExtra.size()); i++) {
            if (plotdata.size()+plotdataExtra.size() > 1) {
                int x0 = i * (getWidth() - border * 2 - labelSize) / (plotdata.size()+plotdataExtra.size() - 1) + border + labelSize;
                int x1 = x0;
                int y0 = getHeight() - border - labelSize;
                int y1 = y0 - pointWidth;
                if ((i % ((int) (((plotdata.size()+plotdataExtra.size()) / 20.0)) + 1)) == 0) {
                    // Vertical grid lines
                	g2d.setColor(gridColor);
                    g2d.drawLine(x0, getHeight() - border - labelSize - 1 - pointWidth, x1, border);
                    // Labels
                    g2d.setColor(Color.BLACK);
                    String xLabel = i + "";
                    FontMetrics metrics = g2d.getFontMetrics();
                    int labelWidth = metrics.stringWidth(xLabel);
                    g2d.drawString(xLabel, x0 - labelWidth / 2, y0 + metrics.getHeight() + 3);
                }
                g2d.drawLine(x0, y0, x1, y1);
            }
        }

        // create x and y axes 
        g2d.drawLine(border + labelSize, getHeight() - border - labelSize, border + labelSize, border);
        g2d.drawLine(border + labelSize, getHeight() - border - labelSize, getWidth() - border, getHeight() - border - labelSize);
   
        
        //Drawing the lines between the points
        g2d.setStroke(GRAPH_STROKE);
        // For first points
        g2d.setColor(lineColor);
        for (int i = 0; i < graphPoints.size() - 1; i++) {
        	//drawLine(x1,y1,x2,y2)
        	g2d.drawLine(graphPoints.get(i).x, graphPoints.get(i).y, graphPoints.get(i + 1).x, graphPoints.get(i + 1).y);
        }
        // For extra points
        g2d.setColor(lineColorExtra);
        //Connecting data points with dataExtra points
        g2d.drawLine(graphPoints.get(graphPoints.size() - 1).x, graphPoints.get(graphPoints.size() - 1).y, graphPointsExtra.get(0).x, graphPointsExtra.get(0).y);
        for (int i = 0; i < graphPointsExtra.size() - 1; i++) {
        	//drawLine(x1,y1,x2,y2)
        	g2d.drawLine(graphPointsExtra.get(i).x, graphPointsExtra.get(i).y, graphPointsExtra.get(i + 1).x, graphPointsExtra.get(i + 1).y);
        }
       
        
        // Drawing the first points
        g2d.setColor(pointColor);
        for (int i = 0; i < graphPoints.size(); i++) {
        	//fillOval(x,y,radius1,radius2)
        	g2d.fillOval(graphPoints.get(i).x - pointWidth / 2, graphPoints.get(i).y - pointWidth / 2, pointWidth, pointWidth);
        }
        // Drawing the extra points
        g2d.setColor(pointColorExtra);
        for (int i = 0; i < graphPointsExtra.size(); i++) {
        	//fillOval(x,y,radius1,radius2)
        	g2d.fillOval(graphPointsExtra.get(i).x - pointWidth / 2, graphPointsExtra.get(i).y - pointWidth / 2, pointWidth, pointWidth);
        }
       
    }

//    @Override
//    public Dimension getPreferredSize() {
//        return new Dimension(width, heigth);
//    }
    private double getMinVal() {
        double minScore = Double.MAX_VALUE;
        for (Double value : plotdata) {
            minScore = Math.min(minScore, value);
        }
        for (Double value : plotdataExtra) {
            minScore = Math.min(minScore, value);
        }
        return minScore;
    }

    private double getMaxVal() {
        double maxScore = Double.MIN_VALUE;
        for (Double value : plotdata) {
            maxScore = Math.max(maxScore, value);
        }
        for (Double value : plotdataExtra) {
        	maxScore = Math.max(maxScore, value);
        }     
        return maxScore;
    }

    
    // This will redraw the canvas if new score are loaded
    public void setScores(List<Double> values) {
        this.plotdata = values;
        invalidate();
        this.repaint();
    }

   

    
  }