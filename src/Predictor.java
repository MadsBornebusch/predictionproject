import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;


public class Predictor {

	private JFrame frame;
	private String filename;
	private String filenamePredictfile; 
	private String timestampHeader;
	private String dataHeader; 
	private int timestampHeaderIndex; 
	private String timeFormat; 
	
	private String timestampPredictHeader;
	private String dataPredictHeader; 
	private int timestampPredictHeaderIndex; 
	
	private int numberPredictions;
	private int maxLag;
	private String predictionModel; 
	
	private CSVreader CSVtrainingFile = null;
	private CSVreader CSVdataFile = null;
	
	
	
	
	private ArrayList<Double> forecastValues = new ArrayList<Double>();
	
	private DefaultListModel model;
	
	wekatest wekatest1 = new wekatest();
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Predictor window = new Predictor();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Predictor() {
		programGUI();
	}
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void programGUI() {
		// Making the frame
		frame = new JFrame("Predictor");
		frame.setBounds(100, 100, 503, 391);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		
		
		
		/*
		 * BUTTON PREDICT 
		 */
		JButton btnNewButton = new JButton("Predict");
		btnNewButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(CSVdataFile!=null){
					// A file is loaded
					
					// Read the data column of the loaded file
					ArrayList<Double> column = new ArrayList<Double>();
					column = CSVdataFile.readColumn(dataHeader);
								
					// Test if same row is chosen twice
					if( timestampPredictHeader != dataPredictHeader){
						//Test if headers 
						if ((timestampPredictHeader.equals(timestampHeader)) && (dataPredictHeader.equals(dataHeader))){
							// Predict
							forecastValues = wekatest1.forecast(filenamePredictfile,numberPredictions,timestampHeaderIndex, timeFormat);
							// Show the graph
							showGraph(column,forecastValues, filenamePredictfile);
					
							// Displaying forecasted values in list in GUI
							model.clear(); 
							for(int i=0; i<forecastValues.size(); i++){
								model.addElement(forecastValues.get(i).toString()); 
							}
							
						}else{
							JOptionPane.showMessageDialog(frame, "Data file must have same header names as training file \r\nNo predictions were made ");
						}
					}else{
						JOptionPane.showMessageDialog(frame, "Please choose two different columns as header column and data column \r\nNo predictions were made ");
					}
				}else{
					JOptionPane.showMessageDialog(frame, "Please load a file  \r\n No prediction was made");
				}
			}
		});
		btnNewButton.setBounds(173, 319, 131, 23);
		frame.getContentPane().add(btnNewButton);
		

		/*
		 * COMBOBOX dataColumn
		 */
		final JComboBox dataColumn = new JComboBox();
		dataColumn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dataHeader=dataColumn.getSelectedItem().toString();
			}
		});
		dataColumn.setBounds(10, 117, 131, 20);
		frame.getContentPane().add(dataColumn);
		
		
		/*
		 * COMBOBOX timestampColumn
		 */
		final JComboBox timestampColumn = new JComboBox();
		timestampColumn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				timestampHeader=timestampColumn.getSelectedItem().toString();
				timestampHeaderIndex = timestampColumn.getSelectedIndex(); 
			}
		});
		timestampColumn.setBounds(10, 82, 131, 20);
		frame.getContentPane().add(timestampColumn);
	
		
		/*
		 * COMBOBOX number Predictions
		 */
		final JComboBox numberPredictionsBox = new JComboBox();
		numberPredictionsBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if( numberPredictionsBox.getSelectedItem() != null){
					numberPredictions = Integer.parseInt(numberPredictionsBox.getSelectedItem().toString());
				}
				
			}
		});
		numberPredictionsBox.setBounds(203, 172, 69, 20);
		frame.getContentPane().add(numberPredictionsBox);
			
		/*
		 * COMBOBOX maxLag
		 */
		final JComboBox maxLagBox = new JComboBox();
		maxLagBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(maxLagBox.getSelectedItem() != null ){
					maxLag = Integer.parseInt(maxLagBox.getSelectedItem().toString());
				}
			}
		});
		maxLagBox.setBounds(30, 209, 69, 20);
		frame.getContentPane().add(maxLagBox);
		
	
		/*
		 * LABEL Training file name
		 */
		final JLabel lblTrainingFilename = new JLabel("No file loaded");
		lblTrainingFilename.setBounds(10, 38, 131, 14);
		frame.getContentPane().add(lblTrainingFilename);
	
		
		/*
		 * BUTTON LOAD TRAINING FILE
		 */
		JButton btnLoadTrainingFile = new JButton("Load training file");
		btnLoadTrainingFile.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				JFileChooser chooser = new JFileChooser(new File(System.getProperty("user.dir"))); // Create the FileChooser instance 
				int returnVal = chooser.showOpenDialog(null);
				
				if(returnVal != JFileChooser.APPROVE_OPTION)
				{
					// User dismissed the dialog
				}
				else
				{
					//Read from file
					
					File file = chooser.getSelectedFile();
					filename = chooser.getSelectedFile().toString();
					
					
					// Check that the file is a CSV-file
					String extension = "";
					int index = filename.lastIndexOf('.');
					if (index > 0) {
					    extension = filename.substring(index+1);
					}
					if(extension.toLowerCase().equals("csv")){
						
						try 
						{
							CSVtrainingFile = new CSVreader(file.getAbsolutePath());
							String[] headerRow = CSVtrainingFile.ReadHeader();
													
							dataColumn.setModel(new DefaultComboBoxModel(headerRow));
							timestampColumn.setModel(new DefaultComboBoxModel(headerRow));
							
							
							//Length of data file							
							int dataColumnLength = CSVtrainingFile.getColumnLength(headerRow[0]); 
							// Sets the maxLag and possible number of predictions
							if (dataColumnLength>=50){
								// Large File - max lag can be up to 25 % of the file
								String[] possibleMaxLag = new String[((int)((dataColumnLength/100)*25))] ; 
								for(int i=1; i<=((int)((dataColumnLength/100)*25)); i++ ){
									possibleMaxLag[i-1] = String.valueOf(i);
								}
								maxLagBox.setModel(new DefaultComboBoxModel(possibleMaxLag));
								maxLagBox.setSelectedIndex(((int)((dataColumnLength/100)*7)));
							}else{
								// Small file - max lag can be the full file
								String[] possibleMaxLag = new String[dataColumnLength]; 
								for(int i=1; i<=dataColumnLength; i++ ){
									possibleMaxLag[i-1] = String.valueOf(i);
								}
								maxLagBox.setModel(new DefaultComboBoxModel(possibleMaxLag));
								maxLagBox.setSelectedIndex(((int)((dataColumnLength/100)*7)));
							}
							
							
							// Checks if there is at least two columns in the file header
							if(2<=headerRow.length){ 
								// Default makes the two first rows selected in the comboboxes
								timestampColumn.setSelectedIndex(0);
								dataColumn.setSelectedIndex(1);
							}
							//Changes the label to the name of the loaded file:
							lblTrainingFilename.setText(getFilename(filename)); 
							
						}
						catch (IOException e1)
						{
							e1.printStackTrace();
						}
					}else{
						JOptionPane.showMessageDialog(frame, "Unfortunately only .csv files are supported. \r\n Please choose a .csv file");
					}
					
					
							
				}
					
			}
		});
		btnLoadTrainingFile.setBounds(10, 10, 131, 23);
		frame.getContentPane().add(btnLoadTrainingFile);
		
		
		
		/*
		 * LABELS
		 */
		JLabel lblNewLabel = new JLabel("Timestamp column");
		lblNewLabel.setBounds(10, 66, 131, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblDataColumn = new JLabel("Data column");
		lblDataColumn.setBounds(10, 102, 131, 14);
		frame.getContentPane().add(lblDataColumn);
		
		
		/*
		 * SCROLL PANE AND LIST - PREDICTED DATA
		 */
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(328, 38, 149, 246);
		frame.getContentPane().add(scrollPane);
		
		JList predictionList = new JList();
		model = new DefaultListModel();
		predictionList.setModel(model);
		scrollPane.setViewportView(predictionList);
		
		JLabel lblPredictedData = new JLabel("Predicted data");
		lblPredictedData.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPredictedData.setBounds(328, 11, 131, 14);
		frame.getContentPane().add(lblPredictedData);
		
		
		
		/*
		 * COMBOBOX TIMESTAMP DATA COLUMN
		 */
		final JComboBox timestampPredictColumn = new JComboBox();
		timestampPredictColumn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				timestampPredictHeader=timestampPredictColumn.getSelectedItem().toString();
				timestampPredictHeaderIndex = timestampPredictColumn.getSelectedIndex(); 
			}
		});
		timestampPredictColumn.setBounds(173, 82, 131, 20);
		frame.getContentPane().add(timestampPredictColumn);
		
		/*
		 * COMBOBOX DATA DATA COLUMN
		 */
		final JComboBox dataPredictColumn = new JComboBox();
		dataPredictColumn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dataPredictHeader=dataPredictColumn.getSelectedItem().toString();
			}
		});
		dataPredictColumn.setBounds(173, 117, 131, 20);
		frame.getContentPane().add(dataPredictColumn);
		
		
		/*
		 * LABEL Data file name
		 */
		final JLabel lblDataFilename = new JLabel("No file loaded");
		lblDataFilename.setBounds(173, 40, 131, 14);
		frame.getContentPane().add(lblDataFilename);
		
		
		/*
		 * BUTTON LOAD DATA FILE
		 */
		final JButton btnLoadDataFile = new JButton("Load data file");
		btnLoadDataFile.setEnabled(false);
		btnLoadDataFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser(new File(System.getProperty("user.dir"))); // Create the FileChooser instance 
				int returnVal = chooser.showOpenDialog(null);
				
				if(returnVal != JFileChooser.APPROVE_OPTION)
				{
					// User dismissed the dialog
				}
				else
				{
					//Read from file
					
					File file = chooser.getSelectedFile();
					filenamePredictfile = chooser.getSelectedFile().toString();
					
					
					// Check that the file is a CSV-file
					String extension = "";
					int index = filenamePredictfile.lastIndexOf('.');
					if (index > 0) {
					    extension = filenamePredictfile.substring(index+1);
					}
					if(extension.toLowerCase().equals("csv")){
						
						try 
						{
							CSVdataFile = new CSVreader(file.getAbsolutePath());
							String[] headerRow = CSVdataFile.ReadHeader();
													
							dataPredictColumn.setModel(new DefaultComboBoxModel(headerRow));
							timestampPredictColumn.setModel(new DefaultComboBoxModel(headerRow));
							
							// Setting the values in the prediction length comboBox
							int predMaxLength = Math.min(CSVdataFile.getColumnLength(headerRow[0]), maxLag); 
							String[] possiblePredictionLengths = new String[predMaxLength]; 
							for(int i=1; i<=predMaxLength; i++ ){
								possiblePredictionLengths[i-1] = String.valueOf(i);
							}
							numberPredictionsBox.setModel(new DefaultComboBoxModel(possiblePredictionLengths));
							numberPredictionsBox.setSelectedIndex(0);
							
							// Checks if there is at least two columns in the file header
							if(2<=headerRow.length){ 
								// Default makes the two first rows selected in the comboboxes
								timestampPredictColumn.setSelectedIndex(0);
								dataPredictColumn.setSelectedIndex(1);
							}
							// Setting file name label
							lblDataFilename.setText(getFilename(filenamePredictfile));
							
						}
						catch (IOException e1)
						{
							e1.printStackTrace();
						}
					}else{
						JOptionPane.showMessageDialog(frame, "Unfortunately only .csv files are supported. \r\n Please choose a .csv file");
					}	
				}
			}
		});
		btnLoadDataFile.setBounds(173, 10, 131, 23);
		frame.getContentPane().add(btnLoadDataFile);
		
		
		/*
		 * LABEL Training file label
		 */
		final JLabel lblModelTrainedWith = new JLabel("Model not trained");
		lblModelTrainedWith.setBounds(173, 258, 131, 14);
		frame.getContentPane().add(lblModelTrainedWith);
		
		final JLabel lblTrainingFileName = new JLabel("");
		lblTrainingFileName.setBounds(173, 276, 131, 14);
		frame.getContentPane().add(lblTrainingFileName);
		
		
		/*
		 * BUTTON TRAIN MODEL
		 */
		JButton btnTrainModel = new JButton("Train model");
		btnTrainModel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(CSVtrainingFile!=null){
					// A file is loaded
					
					if(timestampHeader != dataHeader){
						
						wekatest1.train(filename,timestampHeader, dataHeader,1,maxLag, timestampHeaderIndex, timeFormat, predictionModel);
											
						btnLoadDataFile.setEnabled(true);
						lblModelTrainedWith.setText("Model trained with:");						
						lblTrainingFileName.setText(getFilename(filename));
						
						// Setting the values in the prediction length comboBox if these are bigger than maxLag
						if(numberPredictionsBox.getItemCount() > maxLag){
							String[] possiblePredictionLengths = new String[maxLag]; 
							for(int i=1; i<=maxLag; i++ ){
								possiblePredictionLengths[i-1] = String.valueOf(i);
							}
							numberPredictionsBox.setModel(new DefaultComboBoxModel(possiblePredictionLengths));
							numberPredictionsBox.setSelectedIndex(0);
						}
						
						
					}else{
						JOptionPane.showMessageDialog(frame, "Please choose two different columns as header column and data column \r\n Training was unsuccesful");
					}
				}else{
					JOptionPane.showMessageDialog(frame, "Please load a file");
				}
				
			}
		});
		btnTrainModel.setBounds(10, 319, 131, 23);
		frame.getContentPane().add(btnTrainModel);
		
		
		
		
		/*
		 * LABELS
		 */
		JLabel label = new JLabel("Data column");
		label.setBounds(173, 102, 131, 14);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("Timestamp column");
		label_1.setBounds(173, 66, 131, 14);
		frame.getContentPane().add(label_1);
		
		
		/*
		 * BUTTON SAVE PREDICTION
		 */
		JButton btnSavePrediction = new JButton("Save prediction");
		btnSavePrediction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser filesaver = new JFileChooser(new File(System.getProperty("user.dir"))); // Create the FileChooser instance 
				int returnVal = filesaver.showSaveDialog(null);
				
				if(returnVal != JFileChooser.APPROVE_OPTION)
				{
					// User dismissed the dialog
				}
				else
				{
					//Write to file
					
					
					filename = filesaver.getSelectedFile().toString();
					
					String extension = "";
					int index = filename.lastIndexOf('.');
					if (index > 0) {
					    extension = filename.substring(index+1);
					}
					if(!extension.toLowerCase().equals("csv")){
						// Not csv
						filesaver.setSelectedFile(new File(filename+".csv"));
					}
					
					File file = filesaver.getSelectedFile();
					
					PrintStream output;
					try {
						output = new PrintStream(file);
						
						output.print(timestampPredictHeader +","+dataPredictHeader+"\r\n");
					for (int i=0; i<model.getSize(); i++) {
						output.print((CSVdataFile.getColumnLength(CSVdataFile.ReadHeader()[0])+1+i)+","+ model.getElementAt(i).toString() +"\r\n" );
					}
					//output.flush(); 
					output.close();
					
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					
				}
			}
		});
		btnSavePrediction.setBounds(328, 319, 131, 23);
		frame.getContentPane().add(btnSavePrediction);
		
		
		/*
		 * COMBOBOX Timestamp format
		 */
		final JComboBox timeformatSelection = new JComboBox();
		timeformatSelection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				timeFormat = timeformatSelection.getSelectedItem().toString();
			}
		});
		// Dateformats in Java: http://docs.oracle.com/javase/6/docs/api/java/text/SimpleDateFormat.html
		timeformatSelection.setModel(new DefaultComboBoxModel(new String[] {"Consecutive numbering", "dd/MM/yyyy HH:mm", "yyyy/MM/dd HH:mm:ss", "dd-MM-yyyy", "yyyy/MM/dd"}));
		timeformatSelection.setSelectedIndex(0);
		timeformatSelection.setBounds(10, 163, 131, 20);
		frame.getContentPane().add(timeformatSelection);
		
		JLabel lblTimeFormat = new JLabel("Time format");
		lblTimeFormat.setBounds(10, 148, 131, 14);
		frame.getContentPane().add(lblTimeFormat);
		
		
		JLabel lblNumberOfPredictions = new JLabel("Number of predictions");
		lblNumberOfPredictions.setBounds(183, 157, 111, 14);
		frame.getContentPane().add(lblNumberOfPredictions);
		
		JLabel lblMaxLag = new JLabel("Max lag");
		lblMaxLag.setBounds(30, 194, 69, 14);
		frame.getContentPane().add(lblMaxLag);
		
		JLabel lblModel = new JLabel("Model");
		lblModel.setBounds(10, 240, 69, 14);
		frame.getContentPane().add(lblModel);
		
		final JComboBox modelSelect = new JComboBox();
		modelSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				predictionModel = modelSelect.getSelectedItem().toString(); 
				
			}
		});
		modelSelect.setModel(new DefaultComboBoxModel(new String[] {"SMO regression", "Multilayer model"}));
		modelSelect.setSelectedIndex(0);
		modelSelect.setBounds(10, 255, 131, 20);
		frame.getContentPane().add(modelSelect);
	}
	
	
	private void showGraph(ArrayList<Double> data, ArrayList<Double> dataExtra, String filename){			
		
		// The real showGraph() should have the scores passed and start here
		
		GraphPanel mainPanel = new GraphPanel(data,dataExtra);
		// mainPanel.setScores(data); // Redraw canvas with new data
        mainPanel.setPreferredSize(new Dimension(800,300));
        
       
        
        JFrame frame2 = new JFrame("Prediction made with the file "+"\""+getFilename(filename)+"\"");
        frame2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame2.getContentPane().add(mainPanel);
        frame2.pack();
        frame2.setLocationRelativeTo(null);
        frame2.setVisible(true);
	}
	
	
	private String getFilename(String filepath){
		 // Finding the filename from full file path
        String shortFilename = "";
		int index = filepath.lastIndexOf(File.separator);
		if (index > 0) {
			shortFilename = filepath.substring(index+1);
		}
		return shortFilename; 
	}
}





