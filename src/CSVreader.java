import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;



public class CSVreader 
{	
	private String filePath;

	
	public CSVreader(String fileName) throws IOException
	{
		filePath = fileName;
	}


	public String[] ReadHeader() throws IOException{
		// Below code is inspired by the example found on: http://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/
		
		String [] thisLine = null;
		BufferedReader buffReader = null;
		String line = "";
		String cvsSplitBy = ",";

		try {

			buffReader = new BufferedReader(new FileReader(filePath));
			line = buffReader.readLine(); 
			thisLine = line.split(cvsSplitBy);


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		if (buffReader != null) {
			try {
				buffReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return thisLine;
	}


	public ArrayList<Double> readColumn(String header){
		// Below code is inspired by the example found on: http://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/

		ArrayList<Double> column = new ArrayList<Double>();
		String [] thisLine = null;
		BufferedReader buffReader = null;
		String line = "";
		String cvsSplitBy = ",";

		try {

			buffReader = new BufferedReader(new FileReader(filePath));

			// Reading the header row
			line = buffReader.readLine();
			thisLine = line.split(cvsSplitBy);
			// Find the column in the header row that matches the desired header
			int columnNumber =-1; 
			for(int i=0; i< thisLine.length; i++){
				if(thisLine[i].equals(header)){
					columnNumber =i;
				}
			}

			// Check if the header row was found
			if(columnNumber<0){
				// Header text not found, returns 0
				column.add((double)0);
			}else{
				// Header text found, read column
				while ((line = buffReader.readLine()) != null) {
					thisLine = line.split(cvsSplitBy);
					column.add(Double.parseDouble(thisLine[columnNumber]));
				} 
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		} 

		if (buffReader != null) {
			try {
				buffReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return column;
	}
	
	
	public int getColumnLength(String header){
		// Below code is inspired by the example found on: http://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/
		int length =0; 
		ArrayList<Double> column = new ArrayList<Double>();
		String [] thisLine = null;
		BufferedReader buffReader = null;
		String line = "";
		String cvsSplitBy = ",";

		try {

			buffReader = new BufferedReader(new FileReader(filePath));

			// Reading the header row
			line = buffReader.readLine();
			thisLine = line.split(cvsSplitBy);
			// Find the column in the header row that matches the desired header
			int columnNumber =-1; 
			for(int i=0; i< thisLine.length; i++){
				if(thisLine[i].equals(header)){
					columnNumber =i;
				}
			}

			// Check if the header row was found
			if(columnNumber<0){
				// Header text not found, returns 0
				length =0; 
			}else{
				// Header text found, read column
				while ((line = buffReader.readLine()) != null) {
					length++; 
				} 
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		} 

		if (buffReader != null) {
			try {
				buffReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return length;
	}

}
